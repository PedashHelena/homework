$(document).ready(function () {
    svg4everybody({});

    $('.header_container').on('click', (function(e){
            e.preventDefault();
            var id = e.target.getAttribute('href');
            console.dir(id);
            $('html, body').animate({scrollTop:$(id).position().top}, 2000);
        })
    );

    var $container = $('.portfolio_works').isotope({
        itemSelector: '.portfolio_item',
        layoutMode: 'masonry',
        masonry: {
            horizontalOrder: true
        }
    });
    $('.portfolio_filter').click(function(){
        var $this = $(this);
        if ( !$this.hasClass('active-filter') ) {
            $this.parents('.portfolio_filters').find('.active-filter').removeClass('active-filter');
            $this.addClass('active-filter');
        }
        var selector = $this.attr('data-filter');
        $container.isotope({  itemSelector: '.portfolio_item', filter: selector });
        return false;
    });

});



